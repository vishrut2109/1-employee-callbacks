/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const path = require("path");
const fs = require("fs");

function employeeCallbacks() {
  const directory = __dirname;

  fs.readFile(path.join(directory, "./data.json"), "utf8", (err, data) => {
    if (err) {
      console.error(new Error(err));
    } else {
      data = JSON.parse(data);
      // retrieving the ids 2,13,23 from data.json
      console.log("File read successfuly");
      const output = Object.entries(data).map((dataEntry) => {
        return dataEntry[1].filter((element) => {
          return element.id === 2 || element.id === 13 || element.id === 23;
        });
      });

      fs.writeFile(
        path.join(directory, "./output1.json"),
        JSON.stringify(output),
        "utf8",
        function (err) {
          if (err) {
            console.error(new Error(err));
          } else {
            console.log(
              "retrieved data for ids 2,13,23 and then stored in output1 file "
            );

            // group data based on companies

            const output2 = Object.entries(data).map((dataEntry) => {
              return dataEntry[1].reduce((group, element) => {
                if (group[element.company]) {
                  group[element.company].push(element);
                } else {
                  group[element.company] = [];
                  group[element.company].push(element);
                }
                return group;
              }, {});
            });

            fs.writeFile(
              path.join(directory, "./output2.json"),
              JSON.stringify(output2),
              "utf8",
              function (err) {
                if (err) {
                  console.error(new Error(err));
                } else {
                  console.log("grouped data based on companies ");

                  //3. Get all data for company Powerpuff Brigade
                  const output3 = Object.entries(data).map((dataEntry) => {
                    return dataEntry[1].filter((element) => {
                      return element.company === "Powerpuff Brigade";
                    });
                  });

                  // writing data to file output3
                  fs.writeFile(
                    path.join(directory, "./output3.json"),
                    JSON.stringify(output3),
                    "utf8",
                    function (err) {
                      if (err) {
                        console.error(new Error(err));
                      } else {
                        console.log(
                          "All data for company Powerpuff written in output3.json "
                        );

                        const output4 = Object.entries(data).map(
                          (dataEntry) => {
                            return dataEntry[1].filter((element) => {
                              return element.id !== 2;
                            });
                          }
                        );

                        fs.writeFile(
                          path.join(directory, "./output4.json"),
                          JSON.stringify(output4),
                          "utf8",
                          function (err) {
                            if (err) {
                              console.error(new Error(err));
                            } else {
                              console.log(
                                "removed entry with id = 2 and stored the result in file output4.json"
                              );

                              // sorted data based on company name and secondary meteric id
                              const output5 = Object.entries(data).map(
                                (dataEntry) => {
                                  return dataEntry[1].sort(
                                    (currElement, nextElement) => {
                                      if (
                                        currElement.company ===
                                        nextElement.company
                                      ) {
                                        if (currElement.id > nextElement.id) {
                                          return 1;
                                        } else if (
                                          currElement.id < nextElement.id
                                        ) {
                                          return -1;
                                        } else {
                                          return 0;
                                        }
                                      } else {
                                        if (
                                          currElement.company >
                                          nextElement.company
                                        ) {
                                          return 1;
                                        } else {
                                          return -1;
                                        }
                                      }
                                    }
                                  );
                                }
                              );

                              fs.writeFile(
                                path.join(directory, "./output5.json"),
                                JSON.stringify(output5),
                                "utf8",
                                function (err) {
                                  if (err) {
                                    console.error(new Error(err));
                                  } else {
                                    console.log(
                                      "sorted data based on company name and secondary meteric id and stored the result in file output5.json"
                                    );

                                    // swap company name for id 92 & 93
                                    const output6 = Object.entries(data).map(
                                      (dataEntry) => {
                                        let temp;
                                        return dataEntry[1].map(
                                          (element, index, arr) => {
                                            if (element.id === 93) {
                                              companyID93 = element.company;
                                              temp = arr.filter((item) => {
                                                return item.id === 92;
                                              });
                                              element.company = temp[0].company;
                                            }
                                            if (element.id === 92) {
                                              element.company = companyID93;
                                            }
                                            return element;
                                          }
                                        );
                                      }
                                    );

                                    fs.writeFile(
                                      path.join(directory, "./output6.json"),
                                      JSON.stringify(output6),
                                      "utf8",
                                      function (err) {
                                        if (err) {
                                          console.error(new Error(err));
                                        } else {
                                          console.log(
                                            "swap company names for id 92 & 93 and stored the result in file output6.json"
                                          );

                                          // add birthday as a new field for even id's
                                          const output7 = Object.entries(
                                            data
                                          ).map((dataEntry) => {
                                            return dataEntry[1].map(
                                              (element) => {
                                                if (element.id % 2 === 0) {
                                                  const date = new Date();

                                                  let day = date.getDate();
                                                  let month =
                                                    date.getMonth() + 1;
                                                  let year = date.getFullYear();
                                                  let currentDate = `${day}-${month}-${year}`;
                                                  element["birthday"] =
                                                    currentDate;
                                                }

                                                return element;
                                              }
                                            );
                                          });

                                          fs.writeFile(
                                            path.join(
                                              directory,
                                              "./output7.json"
                                            ),
                                            JSON.stringify(output7),
                                            "utf8",
                                            function (err) {
                                              if (err) {
                                                console.error(new Error(err));
                                              } else {
                                                console.log(
                                                  "output7.json made successfully"
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              }
            );
          }
        }
      );
    }
  });
}

employeeCallbacks();
